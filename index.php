<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include("meta.php"); ?>
        <title>Regulatory Chile - Consultores</title>
        <link href="images/favicon.ico" rel="icon" type="image/x-icon" />
        <!-- Stylesheet -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/animate.css">
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <!-- font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Andika' rel='stylesheet' type='text/css'>
                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->
        <script>
            function limpiarForm(){
                setTimeout('document.getElementById("formulario").reset()', 1000);
            }
        </script>
    </head>
    <body>
        <div id="page">
            <div class="site-navbar navbar navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="images/regulatory_logo_consultores@2x.png" alt="Logo Regulatory Chile Consultores" width="250"></a>

                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="#main">Inicio</a></li>
                            <li><a href="#about">Quienes Somos</a></li>
                            <li><a href="#portfolio">Servicios</a></li>
                            <li><a href="#team">Equipo</a></li>
                            <li><a href="#contact">Contacto</a></li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
            <main id="main" class="site-main">
            <!-- Carousel -->
            <div id="carousel-example-captions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators wow fadeInUp">
                    <li data-target="#carousel-example-captions" data-slide-to="0" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-example-captions" data-slide-to="2" class="active"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item">
                        <img class="wow pulse" data-wow-duration="3s" alt="1" src="images/bg-3.jpg" alt="bg-1">
                        <div class="carousel-caption">
                            <img src="images/RC.png" alt="" class="hidden-md hidden-sm hidden-xs">
                            <h3 class="wow bounceInLeft" data-wow-duration="2s">Confidencialidad</h3>
                        </div>
                    </div>
                    <div class="item">
                        <img class="wow pulse" data-wow-duration="3s" alt="2" src="images/bg-22.jpg" alt="bg-2">
                        <div class="carousel-caption">
                            <img src="images/RC.png" alt="" class="hidden-md hidden-sm hidden-xs">
                            <h3 class="wow bounceInLeft" data-wow-duration="2s">Compromiso</h3>
                        </div>
                    </div>
                    <div class="item active">
                        <img class="wow pulse" data-wow-duration="3s" alt="3" src="images/bg-5.jpg" alt="bg-3">
                        <div class="carousel-caption">
                            <img src="images/RC.png" alt="" class="hidden-md hidden-sm hidden-xs">
                            <h3 class="wow bounceInLeft" data-wow-duration="2s">Experiencia</h3>
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-captions" data-slide="prev">
                <span class="fa fa-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-captions" data-slide="next">
                <span class="fa fa-chevron-right"></span>
                </a>
            </div>
            <!-- ABOUT -->
            <section id="about">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Nosotros</h3>
                        </div>
                        <div class="col-md-6 col-sm-6 wow bounceInLeft">
                            <p>Somos un equipo de profesionales Químicos Farmacéuticos con amplia y comprobada experiencia en todos los ámbitos de la industria cosmética y farmacéutica, lo cual nos permite ofrecer un servicio de asesoría técnica de excelencia, confiable e integral en las áreas de Asuntos Regulatorios, Desarrollo de Productos y Asuntos Comerciales.</p>
                        </div>
                        <div class="col-md-6 col-sm-6 wow bounceInRight">
                            <p>Nuestra empresa se orienta a productos cosméticos, farmacéuticos, dispositivos médicos, desinfectantes, alimentos, plaguicidas, veterinarios, entre otros.</p>
                            <p class="icon-about">
                            <a href="#"><i class="fa fa-facebook-square"></i></a>
                            <a href="#"><i class="fa fa-twitter-square"></i></a>
                            <a href="#"><i class="fa fa-google-plus-square"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            <!-- portfolio -->
            <section id="portfolio">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Servicios</h3>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInUp">
                            <div class="imgport" data-toggle="modal" data-target="#prod_cosm">
                                <span class="wow fadeInDown" data-wow-duration="0.5s">ver</span>
                                <img class="thumbnail" src="images/makeup.jpg" alt="PRODUCTOS COSMÉTICOS">
                            </div>
                            <h4 data-toggle="modal" data-target="#prod_cosm" style="cursor:pointer;">PRODUCTOS COSMÉTICOS</h4>
                            <p>&nbsp;</p>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInLeft">
                            <div class="imgport" data-toggle="modal" data-target="#prod_farma">
                                <span class="wow fadeInDown" data-wow-duration="0.5s">ver</span>
                                <img class="thumbnail" src="images/pharma.jpg" alt="PRODUCTOS FARMACÉUTICOS">
                            </div>
                            <h4 data-toggle="modal" data-target="#prod_farma" style="cursor:pointer;">PRODUCTOS FARMACÉUTICOS</h4>
                            <p>&nbsp;</p>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInRight">
                            <div class="imgport" data-toggle="modal" data-target="#disp_med">
                                <span class="wow fadeInDown" data-wow-duration="0.5s">ver</span>
                                <img class="thumbnail" src="images/disp_med.jpg" alt="3">
                            </div>
                            <h4 data-toggle="modal" data-target="#disp_med" style="cursor:pointer;">DISPOSITIVOS MÉDICOS</h4>
                            <p>&nbsp;</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4 wow bounceInLeft">
                            <div class="imgport" data-toggle="modal" data-target="#desinf">
                                <span class="wow fadeInDown" data-wow-duration="0.5s">ver</span>
                                <img class="thumbnail" src="images/desinfect.jpg" alt="1">
                            </div>
                            <h4 data-toggle="modal" data-target="#desinf" style="cursor:pointer;">DESINFECTANTES</h4>
                            <p>&nbsp;</p>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInLeft">
                            <div class="imgport" data-toggle="modal" data-target="#alim">
                                <span class="wow fadeInDown" data-wow-duration="0.5s">ver</span>
                                <img class="thumbnail" src="images/suplementos.jpg" alt="1">
                            </div>
                            <h4 data-toggle="modal" data-target="#alim" style="cursor:pointer;">ALIMENTOS</h4>
                            <p>&nbsp;</p>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInUp">
                            <div class="imgport" data-toggle="modal" data-target="#pesti">
                                <span class="wow fadeInDown" data-wow-duration="0.5s">ver</span>
                                <img class="thumbnail" src="images/pesti.jpg" alt="2">
                            </div>
                            <h4 data-toggle="modal" data-target="#pesti" style="cursor:pointer;">PLAGUICIDAS</h4>
                            <p>&nbsp;</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-4 wow bounceInRight">
                            <div class="imgport" data-toggle="modal" data-target="#med_vet">
                                <span class="wow fadeInDown" data-wow-duration="0.5s">ver</span>
                                <img class="thumbnail" src="images/vet.jpg" alt="3">
                            </div>
                            <h4 data-toggle="modal" data-target="#med_vet" style="cursor:pointer;">PRODUCTOS VETERINARIOS</h4>
                            <p>&nbsp;</p>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInUp">
                            <div class="imgport" data-toggle="modal" data-target="#ases_com">
                                <span class="wow fadeInDown" data-wow-duration="0.5s">ver</span>
                                <img class="thumbnail" src="images/2.jpg" alt="2">
                            </div>
                            <h4 data-toggle="modal" data-target="#ases_com" style="cursor:pointer;">ASESORÍAS ASUNTOS COMERCIALES</h4>
                            <p>&nbsp;</p>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInRight">

                        </div>
                    </div>

                </div>
            </section>

            <!-- team -->
            <section id="team">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Equipo</h3>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInLeft">
                            <div class="imgteam">
                                <ul class="wow fadeInUp">
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="tel:+56996437189"><i class="fa fa-mobile"></i></a></li>
                                </ul>
                                <img src="images/avatar2.jpg" alt="Loreto González">
                            </div>
                            <h4>Loreto González</h4>
                            <p>Químico Farmacéutico<br>
                                Co-Founder & Consultor</p>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInLeft">
                            <div class="imgteam">
                                <ul class="wow fadeInUp">
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="+56993185944"><i class="fa fa-mobile"></i></a></li>
                                </ul>
                                <img src="images/avatar2.jpg" alt="Ana Rosa Montt">
                            </div>
                            <h4>Ana Rosa Montt</h4>
                            <p>Químico Farmacéutico<br>
                                Co-Founder & Consultor</p>
                        </div>
                        <div class="col-md-4 col-sm-4 wow bounceInLeft">
                            <div class="imgteam">
                                <ul class="wow fadeInUp">
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="+56996443288"><i class="fa fa-mobile"></i></a></li>
                                </ul>
                                <img src="images/avatar2.jpg" alt="Ana Rosa Montt">
                            </div>
                            <h4>Fernando Muñoz</h4>
                            <p>Químico Farmacéutico<br>
                                Asesor Comercial</p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="contact">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Contacto</h3>
                        </div>
                    </div>
                </div>
                <div class="fluid-wrapper">
                    <!-- IFRAME MAPS -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3329.33109484741!2d-70.55522933154435!3d-33.44067946662268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662ce5cb0c1b2d1%3A0x8b5237fc92d92e7a!2sMateo+de+Toro+y+Zambrano+1491%2C+La+Reina%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses-419!2scl!4v1449698961638" width="600" height="350" frameborder="0" style="border:0"></iframe>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 wow bounceInLeft">
                            <br>
                            <p class="mail"> <i class="fa fa-map-marker"></i> &nbsp; Mateo de Toro y Zambrano # 1491, of. 402 <br> &nbsp; La Reina, Santiago - Chile. </p>
                            <p class="mail"> <i class="fa fa-phone-square"></i>&nbsp; <a href="tel:+56229485413">+56229485413</a>
                                | <a href="tel:+56229556931">+56229556931</a> </p>
                        </div>
                        <div class="col-md-7 col-sm-7 wow bounceInRight" id="form" style="visibility:visible; display:block;">
                            <form role="form" class="frm" id="formulario" method="post" action="">
                                <div class="col-md-12">
                                    <div id="success"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Name">Nombre *</label>
                                        <input type="text" class="form-control" name="name" id="Name" placeholder="Nombre" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Email">Email *</label>
                                        <input type="email" class="form-control" Name="email" id="Email" placeholder="Email" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="Message">Mensaje *</label>
                                        <textarea name="message" id="Message" class="form-control" rows="3" placeholder="Escriba su mensaje" required></textarea>
                                    </div>
                                    <input type="submit" id="submit" name="submit" value="Enviar Mensaje" class="btn btn-primary btn-lg btn-block btn-biru" onclick="limpiarForm()" />
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </section>
            </main>
            <footer>
                Regulatory Chile Ltda. &copy; <?php echo date("Y"); ?>
            </footer>
        </div>
                <!-- Scroll to top -->
                <div class="scroll-to-top affix" data-spy="affix" data-offset-top="200"><a href="#page" class="smooth-scroll"><i class="fa fa-arrow-up"></i></a></div>

                <!-- Modal -->
                <div class="modal fade" id="prod_cosm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">PRODUCTOS COSMÉTICOS</h3>
                            </div>
                            <div class="modal-body" style="font-size: 1.4em;">
                                <ul>
                                    <li>Registros cosméticos</li>
                                    <li>Trámite de Inscripción de Empresas Importadoras</li>
                                    <li>Renovaciones y/o Modificaciones de Registros Cosméticos</li>
                                    <li>Notificaciones de Productos Cosméticos de Higiene, Bajo Riesgo y Odorizantes</li>
                                    <li>Trámites de Eximiciones de Control de Calidad Local para Productos Cosméticos Importados</li>
                                    <li>Tramitación de Uso y Disposición y Certificado de Destinación Aduanera (CDA)</li>
                                    <li>Desarrollo de Productos Cosméticos</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="prod_farma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel">Productos Farmacéuticos</h3>
                    </div>
                    <div class="modal-body" style="font-size: 1.4em;">
                        <ul>
                            <li>Registros Farmacéuticos</li>
                            <li>Trámite de Inscripción de Empresas Importadoras</li>
                            <li>Renovaciones y/o Modificaciones de Registros Farmacéuticos</li>
                            <li>Tramitación de Uso y Disposición y Certificado de Destinación Aduanera (CDA)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

                <div class="modal fade" id="disp_med" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">DISPOSITIVOS MEDICOS</h3>
                            </div>
                            <div class="modal-body" style="font-size: 1.4em;">
                                <ul>
                                    <li>Registro de Dispositivos Médicos</li>
                                    <li>Inscripción de Empresas Fabricantes, Exportadoras, Importadoras y Distribuidoras junto a la lista de dispositivos médicos que comercializan</li>
                                    <li>Gestión de Análisis para Dispositivos Médicos</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="desinf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">DESINFECTANTES</h3>
                            </div>
                            <div class="modal-body" style="font-size: 1.4em;">
                                <ul>
                                    <li>Registro de Desinfectante</li>
                                    <li>Renovaciones y/o Modificaciones de Registro de Desinfectante</li>
                                    <li>Tramitación de Uso y Disposición y Certificado de Destinación Aduanera (CDA)</li>
                                    <li>Trámite de Inscripción de Empresa Importadoras</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="alim" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">ALIMENTOS</h3>
                            </div>
                            <div class="modal-body" style="font-size: 1.4em;">
                                <ul>
                                    <li>Tramitación de Uso y Disposición y Certificado de Destinación Aduanera ante Seremi</li>
                                    <li>Internación de alimentos con desarrollo de rótulos</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="pesti" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">PLAGUICIDAS</h3>
                            </div>
                            <div class="modal-body" style="font-size: 1.4em;">
                                <ul>
                                    <li>Registro de plaguicidas nuevos y biológicos</li>
                                    <li>Registro de plaguicidas similares o fitoplaguicida</li>
                                    <li>Renovación de registro de productos plaguicidas de uso sanitario y doméstico</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="med_vet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">PRODUCTOS VETERINARIOS</h3>
                            </div>
                            <div class="modal-body" style="font-size: 1.4em;">
                                <ul>
                                    <li>Registro de medicamentos veterinarios</li>
                                    <li>Renovaciones y/o Modificaciones de Registros  de medicamentos veterinarios</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="ases_com" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel">ASESORIAS ASUNTOS COMERCIALES </h3>
                            </div>
                            <div class="modal-body" style="font-size: 1.4em;">
                                <ul>
                                    <li>Estudios de mercado
                                        <ul>
                                            <li>Farmacéuticos – sector privado – sector público</li>
                                            <li>Cosméticos – dermocosméticos</li>
                                            <li>Evaluación de proyectos</li>
                                        </ul>
                                    </li>
                                    <li>Demanda
                                        <ul>
                                            <li>Estimación de Demanda</li>
                                        </ul>
                                    </li>
                                    <li>Análisis de Fortalezas y Debilidades
                                        <ul>
                                            <li>Escenarios externos: Oportunidades y amenazas</li>
                                            <li>Diagnóstico interno : Fortalezas y debilidades</li>
                                        </ul>
                                    </li>
                                    <li>Capacitaciones
                                        <ul>
                                            <li>Técnicas de productos</li>
                                            <li>Técnicas de ventas</li>
                                        </ul>
                                    </li>
                                    <li>Contacto con canales de distribución
                                        <ul>
                                            <li>Farmacias</li>
                                            <li>Sistema público</li>
                                            <li>Retail</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>



                <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
                <!-- Include all compiled plugins (below), or include individual files as needed -->
                <script src="js/bootstrap.min.js"></script>
                <script src="js/jquery.singlePageNav.js"></script>
                <script src="js/main.js"></script>
                <script src="js/wow.min.js"></script>
                <script>new WOW().init();</script>

                <?php include_once("analyticstracking.php") ?>
            </body>
        </html>
